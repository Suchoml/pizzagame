﻿using System.Collections;
using UnityEngine;

public class Customer : MonoBehaviour
{
    private const string EAT_BOOLEAN = "isEatPizza";

    [SerializeField] private TriggerCustomer triggerCustomer;
    [SerializeField] private Transform pizzaSpawnPoint;
    [SerializeField] private Animator animatorCustomer;

    private Pizza currentPizza;
    private bool isPlayingAnimation = false;
    private int counter = 0;

    private void Start()
    {
        StartCoroutine(SearchPizza());
    }

    public void EndEatAnimation()
    {
        isPlayingAnimation = true;
    }

    private IEnumerator SearchPizza()
    {
        while (true)
        {
            yield return null;

            if (currentPizza == null && triggerCustomer.SelectedPizza != null)
            {
                currentPizza = triggerCustomer.SelectedPizza;
                currentPizza.transform.parent = pizzaSpawnPoint;
                currentPizza.transform.localPosition = Vector3.zero;
                yield return StartCoroutine(Eat());
            }
        }
    }

    private IEnumerator Eat()
    {
        animatorCustomer.SetBool(EAT_BOOLEAN, true);
        
        while (counter < currentPizza.CountPieces)
        {
            isPlayingAnimation = false;

            while (!isPlayingAnimation)
            {
                yield return null;
            }

            currentPizza.Eat(counter);
            counter++;
        }

        counter = 0;
        AppManager.Instance.AddCoins(currentPizza.Money);
        animatorCustomer.SetBool(EAT_BOOLEAN, false);
        Destroy(currentPizza.gameObject);
        currentPizza = null;

        yield return null;
    }
}
