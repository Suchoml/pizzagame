﻿using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textCountCoins;
    
    public void SetCoins(int coins)
    {
        textCountCoins.text = coins.ToString();
    }

}
