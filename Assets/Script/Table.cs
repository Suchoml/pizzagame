﻿using System.Collections.Generic;
using UnityEngine;

public class Table : MonoBehaviour
{
    private const string ARROW_ANIMATION = "Arrow_Animation";

    [SerializeField] private GameObject Arrow;
    [SerializeField] private GameObject Parent;
    [SerializeField] float differenceBetweenArrows = 0.05f;

    private List<Transform> transformArrows;


    void Start()
    {
        transformArrows = new List<Transform>();
        CreateMultipleObjects();
    }
    
    private void CreateMultipleObjects()
    {
        float normilizedTime = 0;

        while (normilizedTime < 1)
        {
            normilizedTime += differenceBetweenArrows;
            GameObject arrow = GameObject.Instantiate(Arrow, Arrow.transform.position, Quaternion.identity, Parent.transform);
            transformArrows.Add(arrow.transform);
            Animator arrowAnimator = arrow.GetComponent<Animator>();
            SetFrame(arrowAnimator, normilizedTime);
        }
    }

    void SetFrame(Animator arrowAnimator, float normilizedTime)
    {
        arrowAnimator.speed = 0.5f;
        arrowAnimator.Play(ARROW_ANIMATION, 0, normilizedTime);
    }

    public Transform FindingNearArrows(Transform spawnPoint, bool isLeft)
    {
        Transform currentObj = null;
        float distanceToCurrentArrow = Mathf.Abs(transformArrows[0].transform.position.y - spawnPoint.transform.position.y);
        float tempDistance;

        for (int i = 1; i < transformArrows.Count; i++)
        {
            if (transformArrows[i].localEulerAngles.z == (isLeft ? -180 : 0))
            {
                continue; 
            }

            tempDistance = Mathf.Abs(transformArrows[i].transform.position.y - spawnPoint.transform.position.y);
            if (tempDistance < distanceToCurrentArrow)
            {
                currentObj = transformArrows[i];
                distanceToCurrentArrow = tempDistance;
            }
        }
        return currentObj;
    }
}
