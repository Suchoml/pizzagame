﻿using System.Collections.Generic;
using UnityEngine;

public class Pizza : MonoBehaviour
{
    [SerializeField] private List<GameObject> pieces;

    public int CountPieces
    {
        get
        {
            return pieces.Count;
        }
    }

    public int Money
    {
        get
        {
            return money;
        }
    }

    [SerializeField] private int money = 5;

    public void Eat(int pieceNum)
    {
       if(pieces != null && pieceNum <= pieces.Count)
        {
            pieces[pieceNum].SetActive(false);
        }
    }
}
