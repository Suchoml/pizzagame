﻿using UnityEngine;

public class AppManager : MonoBehaviour
{
    public static AppManager Instance;

    public UIManager UIManager;
    private int coins = 0;
    
    private void Awake()
    {
        Instance = this;
    }

    public void AddCoins(int _money)
    {
        coins += _money;
        UIManager.SetCoins(coins);
    }
}
