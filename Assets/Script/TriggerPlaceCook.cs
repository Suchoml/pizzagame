﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPlaceCook : MonoBehaviour
{
    public bool IsServing
    {
        get
        {
            return collider2Ds.Count == 0;
        }
    }

    private List<Collider2D> collider2Ds = new List<Collider2D>();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collider2Ds.Add(collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collider2Ds.Contains(collision))
        {
            collider2Ds.Remove(collision);
        }
    }
}
