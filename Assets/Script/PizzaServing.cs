﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PizzaServing : MonoBehaviour
{
    [SerializeField] private List<Transform> varietiesOfPizza;
    [SerializeField] private Transform spawn;
    [SerializeField] private float timeDelay = 10f;
    [SerializeField] private bool isLeft = false;
    [SerializeField] private TriggerPlaceCook triggerChef;
    [SerializeField] private Table table;
    private WaitForSeconds waitForSeconds;


    private void Start()
    {
        waitForSeconds = new WaitForSeconds(timeDelay);
        StartCoroutine(DeliveryPizza());
    }

    private IEnumerator DeliveryPizza()
    {
        while (true)
        {
            yield return waitForSeconds;

            while (!triggerChef.IsServing)
            {
                yield return null;
            }

            SpawnPizza();
        }
    }

    private void SpawnPizza()
    {
        Transform spawnObject = table.FindingNearArrows(spawn, isLeft);
        if (spawnObject != null)
        {
            Transform pizza = varietiesOfPizza[UnityEngine.Random.Range(0, varietiesOfPizza.Count)];
            Transform spawnPizza = Instantiate(pizza, spawnObject.position, Quaternion.identity, spawnObject.GetChild(0));
            spawnPizza.localPosition = Vector3.zero;
        }
    }
}
