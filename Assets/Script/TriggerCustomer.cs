﻿using UnityEngine;

public class TriggerCustomer : MonoBehaviour
{
    public Pizza SelectedPizza { get; private set; }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Pizza selectedPizza = collision.GetComponent<Pizza>();
        if (selectedPizza != null)
        {
            SelectedPizza = selectedPizza;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Pizza selectedPizza = collision.GetComponent<Pizza>();
        if (selectedPizza != null && SelectedPizza == selectedPizza)
        {
            SelectedPizza = null;
        }
    }
}
